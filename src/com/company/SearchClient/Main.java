package com.company.SearchClient;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Scanner;

public class Main {

    private static String baseUrl = "https://www.yandex.ru/";

    public static void main(String[] args) throws Exception {
        // write your code here

        Scanner scanner = new Scanner(System.in);
        URL url;


        while (true) {
            System.out.println("Waiting query...");
            url = new URL(baseUrl + "search/?text=" + scanner.next());
            System.out.println("Your query: " + url.getQuery());
            BufferedReader bufferedReader =
                    new BufferedReader(
                            new InputStreamReader(url.openStream()
                            ));
            String inputLine;
            while ((inputLine = bufferedReader.readLine()) != null) {
                System.out.println(inputLine);
            }
            bufferedReader.close();
        }

    }
}
