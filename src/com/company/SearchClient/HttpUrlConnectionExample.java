package com.company.SearchClient;

import javax.sound.midi.Soundbank;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by xw on 15.06.17.
 */
public class HttpUrlConnectionExample {

    private static String baseUrl = "https://www.yandex.ru/search/?text=";

    public static void main(String[] args) throws Exception {

        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Waiting query...");
            URL url = new URL(baseUrl + scanner.next());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("GET");
            System.out.println("Sending GET request to " + url);
            System.out.println("Response:");

            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream())
            );

            StringBuffer response = new StringBuffer();
            String inputLine;
            while ((inputLine = bufferedReader.readLine()) != null) {
                response.append(inputLine);
            }
            bufferedReader.close();
            System.out.println(response.toString());
        }
    }
}
